#!/usr/bin/env bash

RETURN_CODE=0
for file in $(find sources -name '*.py'); do
    echo "[INFO] Analyzing $file"
    if [[ "$(python3 -m py_compile "$file")" -ne 0 ]]; then
        echo "[ERROR] Bad code into $file"
    fi
    RETURN_CODE=$((RETURN_CODE ++))
done
if [[ "$RETURN_CODE" -ne 0 ]]; then
    echo "[ERROR] Bad code"
    exit 1
fi
