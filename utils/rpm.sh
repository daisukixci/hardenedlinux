#!/usr/bin/env bash

echo "Get last version"
git pull origin "${CI_COMMIT_REF_NAME}"

echo "Build version number $VERSION"
echo "Preparing RPM build environment"
mkdir -p build/{BUILD,BUILDROOT,RPMS,SOURCES,SPECS,SRPMS,tmp}
rsync -a sources/ build/SOURCES
cp "specs/${CI_PROJECT_NAME}.spec" build/SPECS/
cd build

echo "Build ${CI_PROJECT_NAME} RPM"
rpmbuild -ba "SPECS/${CI_PROJECT_NAME}.spec"
if [[ $? -ne 0 ]]; then
    exit 1
fi
mv RPMS/x86_64/*.rpm ..
