#!/usr/bin/env bash

VERSION="$(cat VERSION)"

echo "Get last version"
git pull origin "${CI_COMMIT_REF_NAME}"

echo "Build version number ${VERSION}"
echo "Preparing DEB build environment"
mkdir -p "${CI_PROJECT_NAME}/opt/${CI_PROJECT_NAME}"
cp -r sources/* "${CI_PROJECT_NAME}/opt/${CI_PROJECT_NAME}"
cp -r DEBIAN/ "${CI_PROJECT_NAME}/"
mkdir -p "${CI_PROJECT_NAME}"/usr/{man/man1,share}
mv "${CI_PROJECT_NAME}/opt/hardenedlinux/share/applications" "${CI_PROJECT_NAME}/usr/share/"
mv "${CI_PROJECT_NAME}"/opt/hardenedlinux/share/man/* "${CI_PROJECT_NAME}/usr/man/man1"
gzip "${CI_PROJECT_NAME}/usr/man/man1/hardenedlinux.1"

echo "Build ${CI_PROJECT_NAME} DEB"
dpkg-deb --build "${CI_PROJECT_NAME}"
if [[ $? -ne 0 ]]; then
    exit 1
fi
mv "${CI_PROJECT_NAME}.deb" "${CI_PROJECT_NAME}-${VERSION}.deb"
