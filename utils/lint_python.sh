#!/usr/bin/env bash
shopt -s globstar

RETURN_CODE=0
cd sources/bin || exit 1
for file in ./**/*.py; do
    echo "[INFO] Analyzing $file"
    pylint-3 "$file"
    if [[ "$?" -ne 0 ]]; then
        echo "[ERROR] Bad code into $file"
        RETURN_CODE=$((RETURN_CODE + 1))
        echo "actual $RETURN_CODE"
    fi
done
echo "Final $RETURN_CODE"
if [[ "$RETURN_CODE" -ne 0 ]]; then
    echo "[ERROR] Bad code"
    exit 1
fi
cd - || exit 1
