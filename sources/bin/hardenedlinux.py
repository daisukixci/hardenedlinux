#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Entrypoint for hardenedlinux tool"""
import argparse
import os
import sys
import configparser
import lib.utils as utils

def log(level, msg):
    """
        Log msg according to level
    """
    print("[" + level + "] " + msg)

def check_options(args):
    """
        Check arguments are correct, return True if acceptable
    """
    if not args["gui"] and not args["yaml"]:
        log("err", "Need to have at least yaml or gui")
        return False

    if args["gui"] and args["yaml"]:
        log("err", "Can't get yaml and gui at the same time")
        return False

    if args["iso"] and not os.path.isfile(args["iso"]):
        log("err", args["iso"] + " is not a file")
        return False

    if args["yaml"] and not os.path.isfile(args["yaml"]):
        log("err", args["yaml"] + " is not a file")
        return False
    return True

def main(argv):
    """Parse args and redirect according selected options"""
    arg_parser = argparse.ArgumentParser(argv, description="CLI or GUI tool to generate kickstart \
            or presseed with preconfigured services and hardened")
    arg_parser.add_argument("-g", "--gui", action="store_true", \
            help="Launch the GUI")
    arg_parser.add_argument("-i", "--iso", \
            help="Get iso from internet")
    arg_parser.add_argument("-y", "--yaml", \
            help="Provide the YAML file which describe what OS you want")
    args = vars(arg_parser.parse_args())

    if not any(args.values()):
        arg_parser.error("No arguments provided")

    if not check_options(args):
        sys.exit(1)

    home_path = os.path.dirname(os.path.realpath(sys.argv[0])) + "/.."
    ui_path = home_path + "/share/ui"
    conf_path = home_path + "/etc/hardenedlinux/hardenedlinux.conf"
    config = configparser.ConfigParser()
    config.read(conf_path)

    if args["yaml"]:
        print("YAML provided")
        if utils.detect_yaml(args["yaml"]) == "kickstart":
            print("Kickstart YAML detected")
            import lib.kickstart as kickstart
            kickstart = kickstart.Kickstart().from_yaml(args["yaml"])
            path = "{}/{}".format(config["outputs"]["iso_output_folder"], "test.iso")
            kickstart.generate(path)

    if args["gui"]:
        import lib.gui
        app = lib.gui.QtWidgets.QApplication(sys.argv)
        gui = lib.gui.Gui(ui_path=ui_path)
        gui.show()
        app.exec_()

if __name__ == "__main__":
    main(sys.argv)
