#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    Create a GUI for HardenedLinux tool
"""
__appname__ = 'HardenedLinux'

import gettext
from PyQt5 import QtWidgets, uic, QtCore
gettext.install(__appname__)

LANGUAGES = [["Afrikaans", "af-ZA"], \
        ["Azerbaijani", "az-AZ"], \
        ["Bulgarian", "bg-BG"], \
        ["Catalan", "ca-ES"], \
        ["Czech", "cs-CZ"], \
        ["Danish", "da-DK"], \
        ["German", "de-DE"], \
        ["Greek (Greece)", "el-GR"], \
        ["English (United Kingdom)", "en-GB"], \
        ["Spanish (Spain)", "es-ES"], \
        ["Estonian", "et-EE"], \
        ["Persian", "fa-IR"], \
        ["Finnish", "fi-FI"], \
        ["Francais", "fr_FR"], \
        ["Hungarian", "hu-HU"], \
        ["Italian", "it-IT"], \
        ["Japanese", "ja-JP"], \
        ["Macedonian (F.Y.R.O.M.)", "mk-MK"], \
        ["Norwegian Bokmal", "nb-NO"], \
        ["Dutch", "nl-NL"], \
        ["Polish", "pl-PL"], \
        ["Portuguese (Brazil)", "pt-BR"], \
        ["Portuguese (Portugal) ", "pt-PT"], \
        ["Romanian", "ro-RO"], \
        ["Russian", "ru-RU"], \
        ["Slovak", "sk-SK"], \
        ["Swedish (Sweden) ", "sv-SE"], \
        ["Thai", "th-TH"], \
        ["Turkish", "tr-TR"], \
        ["Ukrainian", "uk-UA"], \
        ["Chinese (Simplified)", "zh-CN"], \
        ["Chinese (Traditional)", "fr_zh-TWFR"]]

KEYBOARDS = [["be-latin1", "Belgian"], \
        ["bg_bds-utf8", "Bulgarian"], \
        ["bg_pho-utf8", "Bulgarian (Phonetic)"], \
        ["br-abnt2", "Brazilian (ABNT2)"], \
        ["cf", "French Canadian"], \
        ["croat", "Croatian"], \
        ["cz-us-qwertz", "Czech"], \
        ["cz-lat2", "Czech (qwerty)"], \
        ["de", "German"], \
        ["de-latin1", "German (latin1)"], \
        ["de-latin1-nodeadkeys", "German (latin1 without dead keys)"], \
        ["dvorak", "Dvorak"], \
        ["dk", "Danish"], \
        ["dk-latin1", "Danish (latin1)"], \
        ["es", "Spanish"], \
        ["et", "Estonian"], \
        ["fi", "Finnish"], \
        ["fi-latin1", "Finnish (latin1)"], \
        ["fr", "French"], \
        ["fr-latin9", "French (latin9)"], \
        ["fr-latin1", "French (latin1)"], \
        ["fr-pc", "French (pc)"], \
        ["fr_CH", "Swiss French"], \
        ["fr_CH-latin1", "Swiss French (latin1)"], \
        ["gr", "Greek"], \
        ["hu", "Hungarian"], \
        ["hu101", "Hungarian (101 key)"], \
        ["is-latin1", "Icelandic"], \
        ["it", "Italian"], \
        ["it-ibm", "Italian (IBM)"], \
        ["it2", "Italian (it2)"], \
        ["jp106", "Japanese"], \
        ["ko", "Korean"], \
        ["la-latin1", "Latin American"], \
        ["mk-utf", "Macedonian"], \
        ["nl", "Dutch"], \
        ["no", "Norwegian"], \
        ["pl2", "Polish"], \
        ["pt-latin1", "Portuguese"], \
        ["ro", "Romanian"], \
        ["ru", "Russian"], \
        ["sr-cy", "Serbian"], \
        ["sr-latin", "Serbian (latin)"], \
        ["sv-latin1", "Swedish"], \
        ["sg", "Swiss German"], \
        ["sg-latin1", "Swiss German (latin1)"], \
        ["sk-qwerty", "Slovak (qwerty)"], \
        ["slovene", "Slovenian"], \
        ["trq", "Turkish"], \
        ["uk", "United Kingdom"], \
        ["ua-utf", "Ukrainian"], \
        ["us-acentos", "U.S. International"], \
        ["us", "U.S. English"]]

BOOTLOADERS = ["grub"]

class Gui(QtWidgets.QMainWindow):
    """Graphic interface class"""

    def __init__(self, ui_path):
        QtWidgets.QWidget.__init__(self)
        uic.loadUi(ui_path + "/gui.ui", self)

        self.partitionWindow = uic.loadUi(ui_path + "/partitionning.ui")
        self.partitions = []

        #declaration of list of users
        self.users = []

        #declaration of list of post-install scripts
        self.script = []

        for language in LANGUAGES:
            self.cbLanguage.addItem(language[0], QtCore.QVariant(language[1]))

        for keyboard in KEYBOARDS:
            self.cbKeyboard.addItem(keyboard[1], QtCore.QVariant(keyboard[0]))

        self.pbUserAdd.clicked.connect(self.useradd)
        self.pbUserDel.clicked.connect(self.userdel)
        self.pbSubmit.clicked.connect(self.submit)
        self.pbPartitionning.clicked.connect(self.partitionShow)

    def submit(self):
        """Controller button to save configuration"""
        if self.cbUbuntu.isChecked:
            import preseed
        else:
            import kickstart


    def useradd(self):
        """Add a user to a list of users"""
        user = User(self.leUsername.text(), self.lePassword.text(), \
                self.leGroup.text(), self.cbEncrypt.isChecked())
        self.users.append(user)
        self.lwUser.addItem(user.__repr__())
        self.leUsername.setText("")
        self.lePassword.setText("")
        self.leGroup.setText("")
        #self.cbEncrypt


    def userdel(self):
        """Delete a user from list of users"""
        del self.users[self.lwUser.currentRow()]
        self.lwUser.takeItem(self.lwUser.currentRow())

    def scriptimport(self):
        """Import script in list of Post-install scripts"""

    def scriptdel(self):
        """Delete script in list of Post-install scripts"""

    def partitionShow(self):
        """Open ths partition window"""
        self.partitionWindow.show()

    def partitionAdd(self):
        """Add a partition to list of partitions"""
        partition = Partition(self.partitionWindow.leLogvol,\
                self.partitionWindow.leVgname,\
                self.partitionWindow.leName,\
                self.partitionWindow.leFstype,\
                self.partitionWindow.leSize,\
                self.partitionWindow.leFsoption)

        self.partitions.append(partition)
        self.partitionWindow.lwPartition.addItem(partition.__repr__)

        #clear line edit
        self.partitionWindow.leLogvol.setText("")
        self.partitionWindow.leVgname.setText("")
        self.partitionWindow.leName.setText("")
        self.partitionWindow.leFstype.setText("")
        self.partitionWindow.leSize.setText("")
        self.partitionWindow.leFsoption.setText("")

    def partitionDel(self):
        """Delete a partition to list of partitions"""
        del self.partitions[self.lwUser.currentRow()]
        self.lwPartition.takeItem(self.lwUser.currentRow())

class User():
    """informations about users"""
    def __init__(self, username, password, group, isencrypt):
        self.username = username
        self.password = password
        self.group = group
        self.isencrypt = isencrypt

    def __repr__(self):
        return "Login: " + self.username + " Password: " + self.password + " Group: " + self.group

class Partition():
    """informations about users"""
    def __init__(self, logvol, vgname, name, fstype, size, fsoptions, grow=""):
        self.logvol = logvol
        self.vgname = vgname
        self.name = name
        self.fstype = fstype
        self.size = size
        self.fsoptions = fsoptions
        self.grow = grow

    def __repr__(self):
        return ""
