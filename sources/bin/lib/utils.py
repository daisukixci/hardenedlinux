#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    Provides utilities functions
"""
import subprocess
import distutils.dir_util
from distutils.errors import DistutilsFileError
import urllib.request
import requests
from yaml import load
try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader

def read_yaml(yaml_path):
    """
        Read the YAML from path and return the YAML dict
    """
    with open(yaml_path, "r") as yaml_stream:
        yaml_data = load(yaml_stream, Loader=Loader)
        return yaml_data

def mount_iso_file(iso_file, target_path):
    """
        Mount an ISO in a temporary directory
    """
    cmd = "mount -o loop " + iso_file + " " + target_path
    return exec_cmd(cmd)

def umount_path(path):
    """
        Umount path
    """
    return exec_cmd("umount " + path)

def get_file_from_http(url, file_path):
    """
        Get a file with HTTP protocol
    """
    proxies = urllib.request.getproxies()
    get_request = requests.get(url, allow_redirects=True, proxies=proxies, verify=False)
    with open(file_path, "wb") as file_data:
        file_data.write(get_request.content)

def exec_cmd(cmd):
    """
        Execute cmd with Popen splitting cmd by space
    """
    try:
        child = subprocess.Popen(cmd.split(" "), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        (out, err) = child.communicate()
        if child.returncode != 0:
            print("Error executing command: " + cmd)
            print("Return code: " + str(child.returncode))
            print("Stdout: " + str(out))
            print("Stderr: " + str(err))
        return child.returncode
    except OSError as subprocess_error:
        print(subprocess_error)

def copy_tree(src, dst):
    """
        Copy a tree using distutil
    """
    try:
        distutils.dir_util.copy_tree(src, dst)
    except DistutilsFileError as src_path_except:
        print("{} is not a directory: {}".format(src, src_path_except))

def rm_tree(target):
    """
        Delete a tree without confirmation, be cautious
    """
    if target == "/":
        print("I refuse to do your dirty work")
        return 1
    distutils.dir_util.remove_tree(target)
    return 0

def detect_yaml(yaml):
    """
        Detect type of yaml file and return it
    """
    yaml_data = read_yaml(yaml)
    if yaml_data["type"] is None and yaml_data["type"] not in ["kickstart", "kickstarts"]:
        print("Type not defined or unknown")
        return None
    return yaml_data["type"]
