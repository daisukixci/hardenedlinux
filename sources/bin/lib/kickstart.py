#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    Define the object kickstart with method to
    write the file
"""
import crypt
import tempfile
import urllib
import re
import lib.utils as utils

class Kickstart():
    """
        Kickstart class
    """
    def __init__(self, init={"type":"install", "mode": "text", "method": "cdrom"}, \
            eula=True, \
            network={"method": "dhcp", "hostname": "kickstart"}, \
            root_password="!!", \
            repos={}, \
            lang="en_US", \
            kdb="fr", \
            selinux="enforcing", \
            firewall={"state": "enable", "trust": [], "services": [], "port": []}, \
            timezone="Europe/Paris", \
            auth={"method": "shadow", "algo": "sha512"}, \
            user={"admin": {"password": "changeme"}}, \
            bios={'location': 'mbr', 'args': '', 'password': ''}, \
            partitions={"biosboot": {"type": "part", "size": 1, "fstype": "biosboot"}, "boot": {"type": "part", "size": 1024, "fstype": "xfs", "asprimary": True}, "pv_root": {"type": "part", "size": 1, "grow": True}, "vg_root": {"type": "volgroup", "pvname": "pv_root"}, "/": {"type": "logvol", "vgname": "vg_root", "name": "root", "fstype": "xfs", "size": 10240, "fsoptions": ""}, "/home": {"type": "logvol", "vgname": "vg_root", "name": "home", "fstype": "xfs", "size": 10240, "fsoptions": "", "grow": True}, "swap": {"type": "logvol", "vgname": "vg_root", "name": "swap", "options": "--recommended"}}, \
            packages={}, \
            skipx=True, \
            services={}, \
            addons={"com_redhat_kdump": "--disable"}, \
            pre_scripts=[], \
            post_scripts=[], \
            post_scripts_chroot=[], \
            end={"method": "reboot", "args": "--eject"}):
        """
            Kickstart constructor
        """
        self.init = init
        self.eula = eula
        self.network = network
        self.root_password = root_password
        self.repos = repos
        self.lang = lang
        self.kdb = kdb
        self.selinux = selinux
        self.firewall = firewall
        self.timezone = timezone
        self.auth = auth
        self.user = user
        self.bios = bios
        self.partitions = partitions
        self.packages = packages
        self.services = services
        self.skipx = skipx
        self.addons = addons
        self.pre_scripts = pre_scripts
        self.post_scripts = post_scripts
        self.post_scripts_chroot = post_scripts_chroot
        self.end = end

    def __get_init_line(self):
        """
            Return kickstart init line
        """
        init_lines = [self.init["type"], self.init["mode"]]
        args = self.init["method"]
        for arg in self.init:
            if arg not in ("type", "method", "url"):
                args = args + " --" + arg + "=" + self.init[arg]
            elif arg == "url":
                args = args + " --" + arg + " " + self.init[arg]
        init_lines.append(args)
        return init_lines

    def __get_network_line(self):
        """
            Return the network generated line according to the method
        """
        if self.network["method"] == "dhcp":
            network_line = "network --bootproto=dhcp --hostname={} link".format(\
                    self.network["hostname"])
        elif self.network["method"] == "static":
            network_line = "network --bootproto=static --ip={} --netmask={}".format(\
                    self.network["ip"], self.network["netmask"])
            if "gateway" in self.network:
                network_line = network_line + " --gateway={}".format(\
                        self.network["gateway"])
            network_line = network_line + \
                    " --hostname={} link".format(self.network["hostname"])
        else:
            return None
        return network_line

    def __get_repo_lines(self):
        """
            Return the repo lines array
        """
        repos = []
        for repo in self.repos:
            repos.append("repo --name={} --baseurl={}".format(repo, self.repos[repo]))
        if repos:
            return repos
        return None

    def __get_lang_line(self):
        """
            Return the self lang line
        """
        return "lang {}".format(self.lang)

    def __get_kdb_line(self):
        """
            Return the self kdb line
        """
        return "keyboard {}".format(self.kdb)

    def __get_selinux_line(self):
        """
            Return the self selinux line
        """
        return "selinux --{}".format(self.selinux)

    def __get_firewall_line(self):
        """
            Return the self firewall line
        """
        firewall_line = "firewall --" + self.firewall["state"]
        if self.firewall["trust"] is not None:
            for trust in self.firewall["trust"]:
                firewall_line = firewall_line + " --trust " + trust
        if self.firewall["services"] is not None:
            for service in self.firewall["services"]:
                firewall_line = firewall_line + " --" + service
        if self.firewall["port"] is not None:
            for port in self.firewall["port"]:
                firewall_line = firewall_line + " --port=" + port
        return firewall_line

    def __get_timezone_line(self):
        """
            Return the self timezone
        """
        return "timezone --utc {}".format(self.timezone)

    # TODO: Add the support for other auth methods
    #--enablenis
    #--nisdomain
    #--nisserver

    #--enableldap
    #--enableldapauth
    #--ldapserver
    #--ldapbasedn
    #--enableldaptls
    #--disableldaptls

    #--enablekrb5
    #--krb5realm
    #--krb5kdc
    #--krb5adminserver
    #--enablehesiod
    #--hesiodlhs
    #--enablesmbauth
    #--smbservers
    #--smbworkgroup

    #--enableshadow
    #--enablecache
    #--passalgo
    def __get_auth_line(self):
        """
            Return the self auth
        """
        if self.auth["method"] == "shadow":
            return "auth --enableshadow --passalgo {}".format(self.auth["algo"])
        return None

    def __get_bios_line(self):
        """
            Return the bios line
        """
        bios_line = "bootloader --location={}".format(self.bios["location"])
        if "args" in self.bios:
            bios_line = bios_line + " --append=\"{}\"".format(self.bios["args"])
        if "password" in self.bios:
            password = crypt.crypt(self.bios["password"])
            bios_line = bios_line + " --iscrypted --password={}".format(password)
        return bios_line

    # TODO: Add support for shell, uid and homedir
    def __get_user_lines(self):
        """
            Return the users lines
        """
        user_lines = []
        for user in self.user:
            password = crypt.crypt(self.user[user]["password"])
            user_line = "user --name={} --password={} --iscrypted".format(user, password)
            if "groups" in self.user[user]:
                user_line = user_line + " --groups={}".format(self.user[user]["groups"])
            user_lines.append(user_line)
        return user_lines

    def __get_packages_lines(self):
        """
            Return packages lines
        """
        packages_lines = ["%packages --nobase", "@Core --nodefaults"]
        if self.packages:
            for package in self.packages:
                if self.packages[package] == "present":
                    package_line = "{}".format(package)
                elif self.packages[package] == "absent":
                    package_line = "-{}".format(package)
                packages_lines.append(package_line)
        packages_lines.append("%end")
        return packages_lines

    def __get_skipx_line(self):
        """
            Return skipx line if self.skipx is True
        """
        if self.skipx:
            return "skipx"
        return None

    # TODO: improve partitions init management
    def __get_partitions_lines(self):
        """
            Return the partitions lines
        """
        partitions_lines = ["zerombr", "clearpart --all --initlabel"]
        for part in self.partitions:
            if self.partitions[part]["type"] == "part":
                partition_line = "part {}".format(part)
            elif self.partitions[part]["type"] == "volgroup":
                partition_line = "volgroup {} {}".format(part, \
                        self.partitions[part]["pvname"])
            elif self.partitions[part]["type"] == "logvol":
                partition_line = "logvol {} --vgname={} --name={}".format(part, \
                        self.partitions[part]["vgname"], \
                        self.partitions[part]["name"])
            if "fstype" in self.partitions[part]:
                partition_line = partition_line + " --fstype={}".format(\
                        self.partitions[part]["fstype"])
            if "size" in self.partitions[part]:
                partition_line = partition_line + " --size={}".format(\
                        self.partitions[part]["size"])
            if "asprimary" in self.partitions[part]:
                partition_line = partition_line + " --asprimary"
            if "grow" in self.partitions[part]:
                partition_line = partition_line + " --grow"
            if "fsoptions" in self.partitions[part] \
                    and self.partitions[part]["fsoptions"] != "":
                partition_line = partition_line + " --fsoptions=\"{}\"".format(\
                        self.partitions[part]["fsoptions"])
            partitions_lines.append(partition_line)
        return partitions_lines

    def __get_root_line(self):
        """
            Return root line
        """
        if self.root_password != "!!":
            password = crypt.crypt(self.root_password)
        else:
            password = self.root_password
        return "rootpw --iscrypted {}".format(password)

    def __get_services_lines(self):
        """
            Return the services lines
        """
        services_disabled = "services --disabled="
        services_enabled = "services --enabled="
        services_enabled = services_enabled + \
                ",".join([service for service, state in self.services.items() \
                if state == "enabled"])
        services_disabled = services_disabled + \
                ",".join([service for service, state in self.services.items() \
                if state == "disabled"])
        return [services_enabled, services_disabled]

    def __get_eula_line(self):
        """
            Return EULA agreement
        """
        if self.eula:
            return "eula --agreed"
        return ""

    def __get_addons_lines(self):
        """
            Return addon lines
        """
        addon_lines = ["%addon"]
        for addon in self.addons:
            addon_lines.append(addon + " " + self.addons[addon])
        addon_lines.append("%end")
        return addon_lines

    def __get_pre_scripts(self):
        """
            Return the pre scripts lines
        """
        pre_scripts_lines = ["%pre --erroronfail --log=/root/preinstall.log"]
        if self.pre_scripts:
            for pre_script in self.pre_scripts:
                pre_scripts_lines.append("%include " + pre_script)
        pre_scripts_lines.append("%end")
        return pre_scripts_lines

    def __get_post_scripts(self):
        """
            Return the pre scripts lines
        """
        post_scripts_lines = [ \
                "%post --nochroot --erroronfail", \
                "/usr/bin/sleep 5", \
                "if [ -f /root/preinstall.log ]; then", \
                "  /usr/bin/cp /root/preinstall.log /mnt/sysimage/root/", \
                "fi"]
        if self.post_scripts:
            for post_script in self.post_scripts:
                post_scripts_lines.append("%include " + post_script)
        post_scripts_lines.append("%end")
        return post_scripts_lines

    def __get_post_scripts_chroot(self):
        """
            Return the pre scripts lines
        """
        post_scripts_chroot_lines = ["%post --erroronfail --log=/root/postinstall.log"]
        if self.post_scripts_chroot:
            for post_script_chroot in self.post_scripts_chroot:
                post_scripts_chroot_lines.append("%include " + post_script_chroot)
        post_scripts_chroot_lines.append("%end")
        return post_scripts_chroot_lines

    def __get_end_line(self):
        """
            Return the end line
        """
        if "args" in self.end:
            return self.end["method"] + self.end["args"]
        return self.end["method"]

    def get(self):
        """
            Return the kickstart lines
        """
        kickstart_content = self.__get_init_line()
        kickstart_content.append(self.__get_network_line())
        if self.repos:
            kickstart_content = kickstart_content + self.__get_repo_lines()
        kickstart_content.append(self.__get_lang_line())
        kickstart_content.append(self.__get_kdb_line())
        kickstart_content.append(self.__get_timezone_line())
        kickstart_content.append(self.__get_selinux_line())
        kickstart_content.append(self.__get_firewall_line())
        kickstart_content.append(self.__get_eula_line())
        kickstart_content.append(self.__get_auth_line())
        kickstart_content.append(self.__get_root_line())
        if self.user:
            kickstart_content = kickstart_content + self.__get_user_lines()
        kickstart_content = kickstart_content + self.__get_partitions_lines()
        kickstart_content.append(self.__get_bios_line())
        kickstart_content = kickstart_content + self.__get_packages_lines()
        if self.services:
            kickstart_content = kickstart_content + self.__get_services_lines()
        if self.skipx:
            kickstart_content.append(self.__get_skipx_line())
        kickstart_content = kickstart_content + self.__get_addons_lines()
        kickstart_content.append(self.__get_end_line())
        kickstart_content = kickstart_content + self.__get_pre_scripts()
        kickstart_content = kickstart_content + self.__get_post_scripts()
        kickstart_content = kickstart_content + self.__get_post_scripts_chroot()
        return kickstart_content

    @classmethod
    def from_yaml(cls, yaml_path):
        """
            Create a kickstart object from a yaml
        """
        yaml_data = utils.read_yaml(yaml_path)
        return cls(init=yaml_data["init"], \
                eula=yaml_data["eula"], \
                network=yaml_data["network"], \
                root_password=yaml_data["root_password"], \
                repos=yaml_data["repos"], \
                lang=yaml_data["lang"], \
                kdb=yaml_data["kdb"], \
                selinux=yaml_data["selinux"], \
                firewall=yaml_data["firewall"], \
                timezone=yaml_data["timezone"], \
                auth=yaml_data["auth"], \
                user=yaml_data["user"], \
                bios=yaml_data["bios"], \
                partitions=yaml_data["partitions"], \
                packages=yaml_data["packages"], \
                skipx=yaml_data["skipx"], \
                services=yaml_data["services"], \
                addons=yaml_data["addons"], \
                pre_scripts=yaml_data["pre_scripts"], \
                post_scripts=yaml_data["post_scripts"], \
                post_scripts_chroot=yaml_data["post_scripts_chroot"], \
                end=yaml_data["end"])

    def write(self, path):
        """
            Write the kickstart on file describe by path
        """
        with open(path, "w") as file_stream:
            file_stream.writelines(["%s\n" % item for item in self.get()])

    def generate(self, path):
        """
            Generate the kickstart with an iso file to path
        """
        # TODO: Add use a local ISO from conf
        file_link = "/home/daisu/Downloads/centos-7.iso"
        working_dir = tempfile.mkdtemp(prefix="hlf")
        mount_dir = tempfile.mkdtemp(prefix=working_dir + "/mount")
        kickstart_dir = tempfile.mkdtemp(prefix=working_dir + "/kickstart")
        iso_path = working_dir + "/centos_7.iso"

        url_parsed = urllib.parse.urlparse(file_link)
        if url_parsed:
            if url_parsed.scheme == "http" or url_parsed.scheme == "https":
                utils.get_file_from_http(file_link, iso_path)
            else:
                iso_path = file_link
        if utils.mount_iso_file(iso_path, mount_dir):
            return 1
        utils.copy_tree(mount_dir, kickstart_dir)
        if utils.umount_path(mount_dir):
            return 1
        self.write(kickstart_dir + "/kickstart.ks")
        with open(kickstart_dir + "/isolinux/isolinux.cfg") as isolinux_stream:
            isolinux_content = isolinux_stream.read()
        with open(kickstart_dir + "/isolinux/isolinux.cfg", "w") as isolinux_stream:
            isolinux_content = re.sub("quiet", \
                    "quiet inst.ks=cdrom:/kickstart.ks", isolinux_content)
            isolinux_stream.write(isolinux_content)
        cmd = "mkisofs -J -T -o {} -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -R -m TRANS.TBL -graft-points -V 'CentOS_7_x86_64' {}".format(path, kickstart_dir)
        if utils.exec_cmd(cmd):
            return 1
        utils.rm_tree(working_dir)
