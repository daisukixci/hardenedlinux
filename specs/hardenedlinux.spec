%define name            hardenedlinux
%define summary         Hardened Linux
%define version         0.0.0
%define _topdir         %(pwd)
%define source          %{_topdir}/SOURCES
%define home            /opt/hardenedlinux
%define desktop         /usr/share/applications
%define man             /usr/man/man1
%define debug_package   %{nil}
%define _tmppath        %{_topdir}/tmp


Name:       %{name}
Summary:    %{summary}
Version:    %{version}
Release:    1
License:    Proprietary
BuildArch:  x86_64
BuildRoot:  %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
Requires: python3, genisoimage

%description
This package is provided by Airbus CyberSecurity and ESIEA.
It contains hardenedlinux.

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}/%{home}
mkdir -p %{buildroot}/%{desktop}
mkdir -p %{buildroot}/%{man}
cp -rf  %{source}/bin %{buildroot}/%{home}
cp -rf  %{source}/etc %{buildroot}/%{home}
cp -rf  %{source}/share %{buildroot}/%{home}
mv %{source}/share/applications/* %{buildroot}/%{desktop}
mv %{source}/share/man/* %{buildroot}/%{man}
gzip %{buildroot}/%{man}/hardenedlinux.1

%clean
rm -rf %{buildroot}

%files
%defattr(0644,root,root,0755)
%{home}
%{man}/hardenedlinux.1.gz
%{desktop}/hardenedlinux.desktop
%defattr(0755,root,root,0755)
%{home}/bin/hardenedlinux.py
